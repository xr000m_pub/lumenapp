<!DOCTYPE html>
<html>
<head>
    <title>Lumen</title>

    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/main.js"></script>

    <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

    <style>
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
            margin-bottom: 40px;
        }

        .quote {
            font-size: 24px;
        }

        .item {
            color: #204d74;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Lumen.</div>

        <?php foreach ($entities as $entity): ?>
            <div class="item"><?= $entity->name; ?></div>
        <?php endforeach; ?>
    </div>
</div>

<script type="text/javascript">
    Main.index.init();
</script>
</body>
</html>