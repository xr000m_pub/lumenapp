<?php
namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class MainController extends BaseController
{
    public function index()
    {
        $entities = \DB::select("SELECT * FROM entities");
        return view('main/index', ['entities' => $entities]);
    }
}
